# Play Livemasjid mount when triggered by MQTT 

Plays a livemasjid mount when a "started" event is triggered on MQTT 

## Compatibility

| ESP32-LyraT | ESP32-LyraT-MSC |

## Usage

Prepare the audio board:

- Connect speakers or headphones to the board.

Configure the example:

- Select compatible audio board in `menuconfig` > `Audio HAL`.
- Set up the Wi-Fi connection by running `menuconfig` > `Livemasjid Configuration` and filling in `WiFi SSID` and `WiFi Password`.
- Set up the Livemasjid mount by running `menuconfig` > `Livemasjid Configuration` and filling in 'Mount'

Load and run the example:

- The audio board will first connect to the Wi-Fi.
- Then the board will start playing automatically.
